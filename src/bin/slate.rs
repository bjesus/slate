//! # Toolbar, Scrollable Text View and File Chooser
//!
//! A simple text file viewer

extern crate gtk;
extern crate sourceview;

use std::io::prelude::*;
use std::io::BufReader;
use std::fs::File;
use std::env;
use std::process::Command;

use gtk::prelude::*;
use gtk::Builder;

use sourceview::ViewExt;

pub fn main() {
    let args: Vec<String> = env::args().collect();

    if gtk::init().is_err() {
        println!("Failed to initialize GTK.");
        return;
    }
    let glade_src = include_str!("slate.glade");
    let builder = Builder::new();
    builder.add_from_string(glade_src).expect("Couldn't add from string");

    let window: gtk::Window = builder.get_object("window").expect("Couldn't get window");
    window.set_wmclass("slate", "Slate");
    window.set_title("Slate");
    let open_button: gtk::Button = builder.get_object("open_button")
                                              .expect("Couldn't get openbutton");
    let new_button: gtk::Button = builder.get_object("new_button")
                                              .expect("Couldn't get newbutton");
    let save_button: gtk::Button = builder.get_object("save_button")
                                              .expect("Couldn't get savebutton");
    let ln_button: gtk::CheckButton = builder.get_object("ln_button")
                                              .expect("Couldn't get ln_button");
    // let font_button: gtk::ModelButton = builder.get_object("font_button")
    //                                         .expect("Couldn't get font_button");

    let text_view: sourceview::View = builder.get_object("text_view")
                                          .expect("Couldn't get text_view");

    if args.len() > 1 {
      let filename = &args[1];
      // TODO: if the file doesn't exist let's create it
      let file = File::open(&filename).expect("Couldn't open file");
      let mut reader = BufReader::new(file);
      let mut contents = String::new();
      let _ = reader.read_to_string(&mut contents);
      text_view.get_buffer().expect("Couldn't get window").set_text(&contents);
    }

    new_button.connect_clicked(move |_| {
        // That's one hell of a stupid hack
        match std::env::current_exe() {
            Ok(exe_path) => Command::new(exe_path).spawn().expect("error!"),
            Err(_e) => Command::new("false").spawn().expect("error!"),
        };
    });

    let window1 = window.clone();
    let text_view1 = text_view.clone();
    open_button.connect_clicked(move |_| {
        let file_chooser = gtk::FileChooserDialog::new(
            Some("Open File"), Some(&window1), gtk::FileChooserAction::Open);
        file_chooser.add_buttons(&[
            ("Open", gtk::ResponseType::Ok.into()),
            ("Cancel", gtk::ResponseType::Cancel.into()),
        ]);
        if file_chooser.run() == gtk::ResponseType::Ok.into() {
            let filename = file_chooser.get_filename().expect("Couldn't get filename");
            let file = File::open(&filename).expect("Couldn't open file");
            let mut reader = BufReader::new(file);
            let mut contents = String::new();
            let _ = reader.read_to_string(&mut contents);
            text_view1.get_buffer().expect("Couldn't get window").set_text(&contents);
        }

        file_chooser.destroy();
    });

    let window2 = window.clone();
    let text_view2 = text_view.clone();
    save_button.connect_clicked(move |_| {
        let file_chooser = gtk::FileChooserDialog::new(
            Some("Save File"), Some(&window2), gtk::FileChooserAction::Save);
        file_chooser.add_buttons(&[
            ("Save", gtk::ResponseType::Ok.into()),
            ("Cancel", gtk::ResponseType::Cancel.into()),
        ]);
        if file_chooser.run() == gtk::ResponseType::Ok.into() {
            let filename = file_chooser.get_filename().expect("Couldn't get filename");
            let mut file = File::create(&filename).expect("Couldn't open file");
            let buffer = text_view2.get_buffer().expect("Couldn't get window");
            let (s, e) = buffer.get_bounds();
            let tempcontent = buffer.get_text(&s, &e, false);
            file.write_all(&tempcontent.unwrap().as_bytes()).expect("Couldn't get window");
        }
        file_chooser.destroy();
    });

    // let window3 = window.clone();
    // let text_view4 = text_view.clone();
    // font_button.connect_clicked(move |_| {
    //     let font_chooser = gtk::FontChooserDialog::new(
    //         Some("Choose Font"), Some(&window3));
    //     if font_chooser.run() == gtk::ResponseType::Ok.into() {
    //         let new_font =  font_chooser.get_font_map();
    //         // println!(new_font.to_string());
    //         text_view4.set_font_map(new_font);
    //     }
    //     font_chooser.destroy();
    // });

    let text_view3 = text_view.clone();
    ln_button.connect_clicked(move |_| {
        text_view3.set_show_line_numbers(!text_view3.get_show_line_numbers());
    });


    window.connect_delete_event(|_, _| {
        gtk::main_quit();
        Inhibit(false)
    });

    window.show_all();
    gtk::main();
}
