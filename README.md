<img width="30" src="http://svgshare.com/i/3tS.svg"> Slate
=========

Slate is an ultra-simple text editor for GNOME. It's built in Rust and meant
to be as lightweight as possible.

[![pipeline status](https://gitlab.com/bjesus/slate/badges/master/pipeline.svg)](https://gitlab.com/bjesus/slate/commits/master)


<div align="center">
  <img width="300" alt="a poem displayed by Slate" src="https://gitlab.com/bjesus/slate/raw/master/screenshot.png">
</div>

## About

Slate is my experiment in studying Rust. Though very simple, the code is
a work in progress and I'm learning a new thing about it everyday. Since
I was always feeling there's a lack of actively maintained tiny text editors
for the Linux desktop, I decided to just write one myself.

## Installation

Slate doesn't have an official package for any Linux distro (yet!), so you'll
must probably have to compile it from source. Don't worry, it's simple.

### Arch Linux

If you're using Arch Linux, you can use the [slate](https://aur.archlinux.org/packages/slate/) package from AUR.

## Manual Installation

### Prerequisites

Compiling Slate requires the most recent stable Rust compiler; it can be installed with
   [`rustup.rs`](https://rustup.rs/).

### Building

Once all the prerequisites are installed, compiling Slate should be easy:

```sh
cargo build --release
```

If all goes well, this should place a binary at `target/release/slate`.

### Desktop Entry

Many linux distributions support desktop entries for adding applications to
system menus. To install the desktop entry for Slate, run

```sh
sudo cp target/release/slate /usr/local/bin # or anywhere else in $PATH
cp Slate.desktop ~/.local/share/applications
cp slate.svg ~/.local/share/pixmaps
```

## Issues (known, unknown, feature requests, etc)

If you run into a problem with Slate, please file an issue. If you've got a
feature request, feel free to ask about it. Pull requests are always welcome.

## License

Slate is released under the [GNU General Public License v3.0].

[GNU General Public License v3.0]: https://gitlab.com/bjesus/slate/raw/master/LICENSE
